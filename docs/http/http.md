# Mas o que é, afinal, o HTTP?

> [Voltar para Linha do Tempo](../linha-do-tempo.md)

É interessante entendermos como funciona o protocolo HTTP, protocolo que iremos utilizar para trafegar as informações através da aplicação web que iremos criar.

HTTP é um acrônimo para HyperText Transfer Protocol, ou Protocolo de Transferência de HiperTexto. Trata-se de um protocolo que estabelece como deve ocorrer a comunicação entre uma máquina cliente que faz pedidos para uma máquina servidora. Ele é normatizado por uma especificação, a RFC 2616 (http://tools.ietf.org/html/rfc2616 ).

O protocolo HTTP é baseado na comunicação entre uma máquina cliente que faz requisições para uma máquina servidora. Cada pedido que a máquina cliente faz para o servidor é chamado de requisição ou request; ao passo que a resposta do servidor para cada pedido é chamada de resposta ou response.

O protocolo HTTP é utilizado desde a década de 90 em páginas e aplicações Web.

Vamos a um exemplo: vamos acessar a página do Google. Se quisermos acessar a página inicial do Google, devemos digitar em nosso navegador:

http://www.google.com.br

Acima, nós temos um HiperTexto, determinado por uma URL (Uniform Resource Locator – Localizador de Recursos Uniforme). Perceba que de fato vamos utilizar o HTTP para fazer a comunicação com o servidor do Google, pois a URL até se inicia com HTTP! Quando damos o enter para que o browser processe a solicitação, uma requisição HTTP é então encaminhada para os servidores do Google, onde ela será processada.

Toda requisição HTTP é composta basicamente por duas partes distintas: cabeçalho (header) e corpo (body). O cabeçalho contém algumas informações específicas da requisição, como o tipo de resposta esperada do servidor e até mesmo o tempo de timeout. Já o corpo pode conter informações adicionais que o cliente pode enviar para o servidor que estarão atreladas à requisição (request). O corpo não é obrigatório, mas o cabeçalho é.

Quando fazemos uma requisição para o Google, nós vamos ter o request similar ao exibido abaixo:

![Requisição HTTP](images/request-http.png)

Perceba que o cabeçalho da requisição envia para o servidor uma série de informações, por exemplo:

- A URL que gerou a requisição (Request URL);
- O tipo de resposta esperada do servidor (Accept);
- O idioma nativo do browser que disparou a requisição (Accept-Language).

Da parte do request, há uma informação importantíssima que é enviada no cabeçalho: o método de requisição (RequestMethod). Este dado do cabeçalho indica que tipo de ação a URL que foi disparada para o servidor deverá realizar, dando sentido semântico - ou seja, significado - à requisição. O protocolo HTTP tem uma série de métodos, como GET, POST, PUT, DELETE, HEAD, OPTIONS, TRACE e CONNECT. Nós, na maioria do tempo, utilizamos mais os métodos GET, POST, PUT e DELETE. O significado destes métodos está na tabela abaixo:

| Método | Significado semântico                                                                                                                                                                                                                                                                                  |
| ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| GET    | Significa que queremos “pegar” algo no servidor: uma página, por exemplo. Requisições GET fazem com que o servidor devolva algo para o cliente, algo que estava “dentro” do servidor.                                                                                                                  |
| POST   | Significa que estamos querendo incluir alguma coisa no servidor. Por exemplo, se temos uma página de cadastro de usuários, a requisição que vai fazer com que o servidor faça o insert no banco de dados deve ser uma requisição POST, afinal, estamos criando um novo item que vai ficar no servidor. |
| PUT    | Significa que estamos querendo atualizar alguma coisa no servidor.                                                                                                                                                                                                                                     |
| DELETE | Significa que estamos querendo apagar alguma coisa do servidor.                                                                                                                                                                                                                                        |

Voltando à requisição para o Google, verifique que ela é uma requisição com o método GET. Isso significa que os servidores do Google deverão retornar alguma coisa para o cliente que disparou a requisição: neste caso, deverá ser retornado o HTML da página inicial do Google, para que o navegador (o nosso cliente neste caso) possa então desenhar a página para nós.

As respostas também possuem cabeçalho. Vamos analisar o cabeçalho da resposta do servidor do Google.

[Resposta HTTP](images/response-http.png)

Perceba que o servidor também retorna uma série de informações sobre ele. A resposta também contém indicadores sobre o controle de cache que o browser deverá executar (cache-control), o tipo de resposta retornado pelo servidor (content-type) e até mesmo a data em que a requisição foi processada no servidor. Agora, existe um item muito importante no cabeçalho de resposta: trata-se do status da resposta. É através deste status que o cliente sabe se a requisição retornou sucesso ou se algo deu errado.

Os status HTTP também são padronizados pela especificação. Os principais status HTTP que temos são:

| Status | Descrição                                                                                                                                                                                 |
| ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 200    | OK. Significa que o servidor entendeu a requisição e a processou sem problemas.                                                                                                           |
| 302    | Found. Significa que o recurso solicitado de fato existe no servidor (status típico de requisições GET).                                                                                  |
| 401    | Unauthorized. Significa que você tentou acessar algum recurso do servidor que exige autenticação para acesso, e você ainda não realizou este processo.                                    |
| 404    | NotFound. Significa que você solicitou algum recurso no servidor que não existe no lugar que você indicou. Por exemplo: se você tenta acessar alguma página de algum site que não existe. |
| 500    | Internal Server Error. Significa que o servidor encontrou um erro durante o processamento da requisição.                                                                                  |

É através destes status HTTP que o cliente sabe se a requisição que ele disparou deu certo ou não.

O protocolo HTTP ainda possui algumas características que precisamos conhecer:

- O protocolo HTTP é stateless. Isso significa que ele não guarda estado. Por exemplo, você não consegue, somente com o protocolo HTTP, guardar se você acessou determinada página ou não, ou mesmo se um usuário fez o login em sua aplicação web ou não. Isso ocorre porque as requisições HTTP são independentes entre si: quando você faz uma requisição, é aberto um canal de comunicação com o servidor. Por este canal, é trafegada a requisição e a resposta. Logo quando o cliente recebe a resposta, este canal é imediatamente fechado (salvo algumas indicações que podemos fornecer no cabeçalho da requisição). Devido a isso, ele não pode guardar estado, pois esse canal é constantemente aberto e fechado, fora que as requisições ocorrem de maneira isolada (uma requisição não sabe se existe alguma outra requisição sendo feita ou não). Existem estruturas que podemos utilizar para “burlar” esta característica do HTTP que veremos neste curso;
- Como foi dito, o protocolo HTTP é independente. Quando você faz uma requisição para o servidor, ela é tratada de maneira isolada das demais requisições, sendo impossível fazer com que requisições se comuniquem umas com as outras. Por exemplo: vamos imaginar que você faz uma requisição para uma página HTML que possui um texto e uma imagem. O navegador fará no mínimo duas requisições para carregar esta página: uma para recuperar o texto e outra para recuperar a imagem. Porém, apesar de ambas as requisições serem necessárias para montar uma única página, o servidor as entenderá de maneira completamente isolada, cabendo ao cliente juntar ambas as respostas para montar a página solicitada;
- O protocolo HTTP é assíncrono, ou seja: você pode fazer várias requisições ao mesmo tempo. Como estas requisições são independentes, elas também serão tratadas ao mesmo tempo. O servidor também irá devolver as respostas não necessariamente na mesma ordem em que as requisições foram realizadas, ele irá devolver à medida que o processamento for sendo finalizado.

É importante conhecermos estas características do protocolo HTTP para justificarmos uma série de recursos e técnicas que vamos utilizar mais à frente, quando começaremos a criar aplicações web.

Caso queria se aprofundar e conhecer melhor este protocolo (o que pode vir a ser bem interessante), temos um [curso voltado especificamente para o protocolo HTTP](https://www.treinaweb.com.br/curso/introducao-ao-http).
